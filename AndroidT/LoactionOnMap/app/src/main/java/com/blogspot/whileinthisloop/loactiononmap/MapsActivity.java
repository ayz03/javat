package com.blogspot.whileinthisloop.loactiononmap;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    GoogleApiClient client;
    LocationRequest mLocationRequest;
    static final Integer LOCATION = 0x1;
    static final Integer GPS_SETTINGS = 0x7;
    static final String TAG = "MapsActivityDebug";
    PendingResult<LocationSettingsResult> result;
    private Context context;
    private Activity activity;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(TAG,"onCreate() called");
        context = getApplicationContext();
        activity = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Log.d(TAG,"layout created");

        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,LOCATION);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void askForPermission(String permission, Integer requestCode)
    {
        Log.d(TAG,"checking location permission");
        if (ContextCompat.checkSelfPermission(MapsActivity.this, permission) != PackageManager.PERMISSION_GRANTED)
        {
            Log.d(TAG,"permission not granted");
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MapsActivity.this, permission))
            {
                Log.d(TAG,"if previously denied we are asking again");
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(MapsActivity.this, new String[]{permission}, requestCode);

            }
            else
            {
                Log.d(TAG,"requesting to grant permission");
                ActivityCompat.requestPermissions(MapsActivity.this, new String[]{permission}, requestCode);
            }
        }
        else
        {
            Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
            //now call appropraite ones
            Log.d(TAG,"permission was granted previously");
            Log.d(TAG, "now asking user to turn on gps");
            askForGPS();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        Log.d(TAG,"user responded");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(this, "Permission granted by user", Toast.LENGTH_SHORT).show();
            switch (requestCode)
            {
                //Location
                case 1:
                    Log.d(TAG, "now asking user to turn on gps");
                    //Toast.makeText(this, "asking for gps", Toast.LENGTH_SHORT).show();
                    askForGPS();
                    break;
                default:
                    break;
            }
        }
        else
        {
            Toast.makeText(this, "Permission denied by user", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onStart()
    {
        Log.d(TAG, "onStart() called");
        super.onStart();
        client.connect();
    }

    public void onResume()
    {
        Log.d(TAG, "onResume() called");
        super.onResume();
        client.connect();
    }
    @Override
    public void onPause()
    {
        Log.d(TAG, "onPause() called");
        super.onPause();
        LocationServices.FusedLocationApi.removeLocationUpdates(client, this);
        client.disconnect();
    }
    @Override
    public void onStop()
    {
        Log.d(TAG, "onStop() called");
        super.onStop();
        client.disconnect();
    }

    private void askForGPS()
    {
        Log.d(TAG, "in askForGps()");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(5 * 1000) ;       // 10 seconds, in milliseconds

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        builder.setAlwaysShow(true);

        client = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(AppIndex.API)
                .addApi(LocationServices.API)
                .build();

        result = LocationServices.SettingsApi.checkLocationSettings(client, builder.build());
        Log.d(TAG, "5");

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MapsActivity.this, GPS_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private boolean checkGpsPermission()
    {
        Log.d(TAG, "checkGpsPermission() called");
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        Log.d(TAG, "onMapReady() called");
        mMap = googleMap;
        mMap.clear();
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        if(checkGpsPermission()) //means ACCESS FINE LOCATION is permitted
        {
            mMap.setMyLocationEnabled(true);
        }
        else
        {
            onStop();
        }
    }

    //when google API client gets connected reads location and update it on map
    @Override
    public void onConnected(Bundle bundle)
    {
        Log.d(TAG, "onConnected() called");
        if (checkGpsPermission())
        {
            Location location = LocationServices.FusedLocationApi.getLastLocation(client);
            LocationServices.FusedLocationApi.requestLocationUpdates(client, mLocationRequest, this);
            updateLocationOnMap(location);
        }
        else
        {
            onStop();
        }
    }

    //if location gets changed update location
    @Override
    public void onLocationChanged(Location location)
    {
        Log.d(TAG, "onLocationChanged() called");
        updateLocationOnMap(location);
    }
    //if connection gets failed
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
        Log.d(TAG, "onConnectionFailed() called");
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.d(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onConnectionSuspended(int i)
    {}

    private void updateLocationOnMap(Location location)
    {
        Log.d(TAG, "updateLocationOnMap() called");
        Log.d(TAG, location.toString());
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();

        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
        //mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location"));
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("hi..")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        //mMap.clear();    //clears the previous location
        mMap.addMarker(options);
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.clear(); //clears the map at each time of update
        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(location.getLatitude(), location.getLongitude())).zoom(17).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

}
