package com.example.ayaz.safebikepark;

import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        /* DatabaseHandler db = new DatabaseHandler(this);
        // Deleting all previous contacts
        /*List<Contact> contactsDlt = db.getAllContacts();
        for (Contact cn : contactsDlt) {
            db.deleteContact(cn);
        }

        //inserting contacts

        Log.d("Insert: ", "Inserting ..");
        db.addContact(new Contact(0,"Lions", "012345678",23.796182,90.402022));
        db.addContact(new Contact(1,"CafeCyclist", "07896142356",23.751039,90.385776));
        db.addContact(new Contact(2,"CyClelife", "01672105689",23.771000,90.405698));

        // Reading all contacts
        Log.d("Reading: ", "Reading all contacts..");
        List<Contact> contactsRd = db.getAllContacts();

        for (Contact cn : contactsRd) {
            String log = "Id: "+cn.getID()+" ," +
                         "Name: " + cn.getName() + " ," +
                         "Phone: " + cn.getPhoneNumber() + " ,"+
                         "Lattitude: "+ cn.getLattitude() + " ,"+
                         "Longtitude: "+ cn.getLongtitude();
            // Writing Contacts to log
            Log.d("Name: ", log);
        }*/

        final Intent i_parkhouse = new Intent(this, ParkHouse.class);
        ImageButton btnhouse;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnhouse = (ImageButton) findViewById(R.id.imgBtnHouse);

        btnhouse.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(i_parkhouse);
            }
        });

        final Intent i_Map = new Intent(this, MapsActivity.class);
        ImageButton btnMap;
        btnMap = (ImageButton) findViewById(R.id.imgBtnMap);

        btnMap.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(i_Map);
            }
        });
    }
}
