package com.example.ayaz.safebikepark;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;


public class ParkHouse extends AppCompatActivity {

    ImageButton btnOwnerHouse;
    ImageButton btnCareTaker;
    Button btnCnf;
    static final int CAPTURE_HOUSE_PICTURE = 0;
    static final int CAPTURE_CARE_TAKER_PICTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_park_house);

        btnOwnerHouse = (ImageButton) findViewById(R.id.imgBtnOwnerHouse);

        btnOwnerHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAPTURE_HOUSE_PICTURE);
            }
        });

        btnCareTaker = (ImageButton) findViewById(R.id.imgBtnCareTaker);

        btnCareTaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAPTURE_CARE_TAKER_PICTURE);
            }
        });

        Button btnCnf = (Button)findViewById(R.id.btnConfirm);
        btnCnf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Context context = getApplicationContext();
                CharSequence text = "info uploaded \n you are now a Elite Owner .. cheers ";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == CAPTURE_HOUSE_PICTURE)
        {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user captured an image
                Bitmap bp = (Bitmap) data.getExtras().get("data");
                btnOwnerHouse.setImageBitmap(bp);
            }
        }
        if (requestCode == CAPTURE_CARE_TAKER_PICTURE)
        {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user captured an image
                Bitmap bp = (Bitmap) data.getExtras().get("data");
                btnCareTaker.setImageBitmap(bp);
            }
        }
    }
}
