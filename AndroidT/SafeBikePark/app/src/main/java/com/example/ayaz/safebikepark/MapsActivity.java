package com.example.ayaz.safebikepark;

import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;

public class MapsActivity extends FragmentActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMapLongClickListener,
        LocationListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener {

    public static final String TAG = MapsActivity.class.getSimpleName();

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLat;
    private double currentLng;

    private Marker myMarker,spLions,spCafe,spCLE,houseArif,houseBabu,houseAyz,houseReza;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Debug: ", "onCreate()");
        //setting up the layout
        setContentView(R.layout.activity_maps);
        //create & set up google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        // Create & set up the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(60 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(20 * 1000); // 1 second, in milliseconds
        setUpMapIfNeeded();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("Debug: ", "onStart()");
        mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Debug: ", "onCreate()");
        setUpMapIfNeeded();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Debug: ", "onPause()");
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onStop() {
        Log.d("Debug: ", "onStop()");
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    private void setUpMapIfNeeded() {
        Log.d("Debug: ", "setUpMapIfNeeded()");
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.clear();  //clears previous locations

                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMapToolbarEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.setOnMapLongClickListener(this);
                mMap.setOnMarkerClickListener(this);
                mMap.setOnInfoWindowClickListener(this);
            }
        }
    }

    /*################ Google Api client callbacks ################*/
    @Override
    public void onConnected(Bundle bundle) {
        Log.d("Debug: ", "onConnected()");
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        handleNewLocation(location);
    }

    public void onDisconnected() {
        Toast.makeText(this, "GoogleApiClient is disconnected", Toast.LENGTH_LONG);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this,"GoogleApiClient connection is suspended", Toast.LENGTH_LONG);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
            Toast.makeText(this, "Make sure Google Play service is enabled", Toast.LENGTH_LONG);
        }
    }

    /****************** GoogleAPIclient Callbacks *****************/

    /*################# LOcation callbacks ##################*/
    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    private void handleNewLocation(Location location) {
        Log.d("Debug: ", "handleNewLocation()");
        Log.d(TAG, location.toString());

        currentLat = location.getLatitude();
        currentLng = location.getLongitude();

        mMap.clear(); //clears the map at each time of update
        drawServicePoints();
        drawHousePoints(location);

        myMarker = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .title("I am Here!"));

        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(location.getLatitude(), location.getLongitude())).zoom(12).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void drawServicePoints()
    {
        spLions = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(new LatLng(23.796182, 90.402022))
                .title("Lions Cycle Store")
                .snippet("All Services Available"));
        spLions.showInfoWindow();

        spCafe = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(new LatLng(23.751039, 90.385776))
                .title("Cafe Cyclist")
                .snippet("wash available"));
        spCafe.showInfoWindow();

        spCLE = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(new LatLng(23.771000, 90.405698))
                .title("CycleLife Exclusive")
                .snippet("new Veloce Bikes"));
        spCLE.showInfoWindow();
    }

    private void drawHousePoints(Location loc)
    {
        houseArif = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .position(new LatLng(loc.getLatitude() + 0.033, loc.getLongitude() + 0.031))
                .title("Arifur Rahman Khan")
                .snippet("parking time: 10:00AM to 8:00 PM"));
        houseArif.showInfoWindow();

        houseAyz = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .position(new LatLng(loc.getLatitude() - 0.0503, loc.getLongitude() + 0.0609))
                .title("Hassin Ayaz")
                .snippet("Rate : 10TK/2hrs"));
        houseAyz.showInfoWindow();

        houseBabu = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .position(new LatLng(loc.getLatitude()-0.042, loc.getLongitude()-0.048))
                .title("Shakhawat Ullah")
                .snippet("Care Taker Mobile no: 0178965231"));
        houseBabu.showInfoWindow();

        houseReza = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .position(new LatLng(loc.getLatitude()+0.0593, loc.getLongitude()-0.0613))
                .title("Rezaual Hasan")
                .snippet("Park Full"));
        houseReza.showInfoWindow();
    }

    /********************* Location Callbacks **********************/

    @Override
    public void onMapLongClick(LatLng latLng) {
        String locationStr = latLng.toString();
        Toast.makeText(this, locationStr, Toast.LENGTH_LONG).show();
        }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if (!marker.equals(myMarker))
        {
            LatLng latLng = marker.getPosition();
            double destinationLatitude = latLng.latitude;
            double destinationLongitude = latLng.longitude;

            String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f", currentLat, currentLng, destinationLatitude, destinationLongitude);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(intent);
        }
    }
}