package com.example.ayaz.safebikepark;

/**
 * Created by Ayaz on 1/1/2016.
 */
public class Contact {
    //private variables
    int _id;
    String _name;
    String _phone_number;
    Double lat;
    Double lng;
    // Empty constructor
    public Contact(){

    }
    // constructor
    public Contact(int id, String name, String _phone_number, Double argLat, Double argLng){
        this._id = id;
        this._name = name;
        this._phone_number = _phone_number;
        this.lat = argLat;
        this.lng = argLng;
    }

    // constructor
    public Contact(String name, String _phone_number, Double argLat, Double argLng){
        this._name = name;
        this._phone_number = _phone_number;
        this.lat = argLat;
        this.lng = argLng;
    }
    // getting ID
    public int getID(){
        return this._id;
    }

    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting name
    public String getName(){
        return this._name;
    }

    // setting name
    public void setName(String name){
        this._name = name;
    }

    // getting phone number
    public String getPhoneNumber(){
        return this._phone_number;
    }

    // setting phone number
    public void setPhoneNumber(String phone_number){
        this._phone_number = phone_number;
    }

    public void setLattitude(Double argLat)
    {
        this.lat = argLat;
    }

    public Double getLattitude()
    {
        return this.lat;
    }

    public void setLongtitude(Double argLng)
    {
        this.lng = argLng;
    }

    public Double getLongtitude()
    {
        return this.lng;
    }

}
