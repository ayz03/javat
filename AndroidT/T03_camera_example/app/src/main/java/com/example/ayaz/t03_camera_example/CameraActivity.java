package com.example.ayaz.t03_camera_example;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;


public class CameraActivity extends AppCompatActivity {

    Button btnCamera;
    ImageView imgCaptured;
    static final int CAPTURE_PICTURE_T03 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        btnCamera = (Button)findViewById(R.id.button_camera);
        imgCaptured = (ImageView)findViewById(R.id.imageView_captured_img);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAPTURE_PICTURE_T03);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == CAPTURE_PICTURE_T03) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user captured an image
                Bitmap bp = (Bitmap) data.getExtras().get("data");
                imgCaptured.setImageBitmap(bp);
            }
        }
    }
}
