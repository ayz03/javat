package com.example.ayaz.t02_broadcast_example;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Ayaz on 12/5/2015.
 */

//this receiver component should be defined at manifest with the intent string defined
// in order to let the app know what intent it will receive
public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Intent Detected.", Toast.LENGTH_LONG).show();
    }
}
