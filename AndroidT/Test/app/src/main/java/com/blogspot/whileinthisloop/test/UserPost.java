package com.blogspot.whileinthisloop.test;

/**
 * Created by Ayaz on 12/22/2015.
 */
import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Data model for a post.
 */
@ParseClassName("Posts")
public class UserPost extends ParseObject {

    public void setUser(ParseUser value) {
        put("user", value); }

    public void setAddress(String value) {
        put("address", value);
    }

    public void setLocation(ParseGeoPoint value) {
        put("location", value);}

    public ParseUser getUser() {
        return getParseUser("user");}

    public String getAddress() {
        return getString("address");}

    public ParseGeoPoint getLocation() {
        return getParseGeoPoint("location");}

    public static ParseQuery<UserPost> getQuery() {
        return ParseQuery.getQuery(UserPost.class);
    }
}
