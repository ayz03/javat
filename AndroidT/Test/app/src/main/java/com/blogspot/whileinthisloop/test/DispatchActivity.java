package com.blogspot.whileinthisloop.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.parse.ParseUser;

/**
 * Activity which starts an intent for either the logged in (MainActivity) or logged out
 * (SignUpOrLoginActivity) activity.
 */
public class DispatchActivity extends Activity {

  public DispatchActivity() {
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (ParseUser.getCurrentUser() != null)  // Check if there is current user info
    {
      startActivity(new Intent(this, MainActivity.class)); // Start an intent for the logged in activity
    }
    else
    {
      startActivity(new Intent(this, WelcomeActivity.class));  // Start and intent for the logged out activity
    }
  }

}
