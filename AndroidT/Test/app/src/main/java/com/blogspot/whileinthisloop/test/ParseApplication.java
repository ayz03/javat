package com.blogspot.whileinthisloop.test;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;

/**
 * Created by Ayaz on 12/17/2015.
 */
public class ParseApplication extends Application{
    public static final String YOUR_APPLICATION_ID = "hb0tXRfS5UJD55tvx0au1ElRalIZk2Ex9s04wCog";
    public static final String YOUR_CLIENT_KEY = "g29kWbp8gVsyoPl44zhZMBdUf6w7F1mMhEH4Gzkl";

    @Override
    public void onCreate() {
        super.onCreate();

        // Add your initialization code here
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, YOUR_APPLICATION_ID, YOUR_CLIENT_KEY);
        // Test creation of object
//        ParseObject testObject = new ParseObject("Places");
//        testObject.put("address", "575 north ibrahimpur");
//        testObject.put("cellno", "01672241010");
//        testObject.saveInBackground();
        // ...
    }
}
