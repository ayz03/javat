package com.blogspot.whileinthisloop.petaplant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class NavigatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigator);

        final Intent reqFormIntent = new Intent(this, ReqFormMapActivity.class);
        ImageButton imgBtnReqPlant;
        imgBtnReqPlant = (ImageButton) findViewById(R.id.imgBtnReqPlant);

        imgBtnReqPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(reqFormIntent);
            }
        });

        final Intent gardenIntent = new Intent(this, plantListActivity.class);
        ImageButton btnGarden;
        btnGarden = (ImageButton) findViewById(R.id.imgBtnGarden);

        btnGarden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(gardenIntent);
            }
        });

        final Intent communityIntent = new Intent(this, CommunityActivity.class);
        ImageButton imgBtncomm;
        imgBtncomm = (ImageButton)findViewById(R.id.imgBtnCommunity);

        imgBtncomm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(communityIntent);
            }
        });
    }
}
