/*
Specifically, use the fused location provider to retrieve the device's last known location.
The fused location provider is one of the location APIs in Google Play services.
1. To access the fused location provider, your app's development project must include Google Play services
2. add location permissions. ACCESS_COARSE_LOCATION and ACCESS_FINE_LOCATION.
 */
package com.blogspot.whileinthisloop.petaplant;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ReqFormMapActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    //Buttons related
    ImageButton btnPlantPlace;
    Button btnCnf;
    static final int CAPTURE_PLANT_PLACE = 0;
    //lmap related
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;

    //location related
    private LocationRequest mLocationRequest;
    static final Integer LOCATION = 0x1;
    static final Integer GPS_SETTINGS = 0x7;
    PendingResult<LocationSettingsResult> result;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    double currentLatitude;
    double currentLongitude;

    //permissions related
    private Context context;
    private Activity activity;
    private boolean gpsOn = false;

    //Debug related
    public static final String TAG = ReqFormMapActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(TAG,"onCreate() called");
        context = getApplicationContext();
        activity = this;
        super.onCreate(savedInstanceState);
        //create the layout
        setContentView(R.layout.activity_req_form_with_maps);
        Log.i(TAG, "layout created");

        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,LOCATION);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Buttons initialization
        btnPlantPlace = (ImageButton) findViewById(R.id.imgBtnPlantPlace);
        btnPlantPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAPTURE_PLANT_PLACE);
            }
        });

        btnCnf = (Button) findViewById(R.id.btnConfirm);
        btnCnf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getApplicationContext();
                CharSequence text = "info uploaded \nMany many thanks :)\nPlease wait few days \nwe will bring your plant...";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
        //google API client setup
        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

        /*mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(AppIndex.API).build();
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                  .addLocationRequest(mLocationRequest);

          //**************************
          builder.setAlwaysShow(true); //this is the key ingredient
        //setUpMapIfNeeded();*/
    }

    private void askForPermission(String permission, Integer requestCode)
    {
        Log.d(TAG,"checking location permission");
        if (ContextCompat.checkSelfPermission(ReqFormMapActivity.this, permission) != PackageManager.PERMISSION_GRANTED)
        {
            Log.d(TAG,"permission not granted");
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ReqFormMapActivity.this, permission))
            {
                Log.d(TAG,"if previously denied we are asking again");
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(ReqFormMapActivity.this, new String[]{permission}, requestCode);

            }
            else
            {
                Log.d(TAG,"requesting to grant permission");
                ActivityCompat.requestPermissions(ReqFormMapActivity.this, new String[]{permission}, requestCode);
            }
        }
        else
        {
            Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
            //now call appropraite ones
            Log.d(TAG,"permission was granted previously");
            Log.d(TAG, "now asking user to turn on gps");
            askForGPS();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        Log.d(TAG,"user responded");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(this, "Permission granted by user", Toast.LENGTH_SHORT).show();
            switch (requestCode)
            {
                //Location
                case 1:
                    Log.d(TAG, "now asking user to turn on gps");
                    //Toast.makeText(this, "asking for gps", Toast.LENGTH_SHORT).show();
                    askForGPS();
                    break;
                default:
                    break;
            }
        }
        else
        {
            Toast.makeText(this, "Permission denied by user", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onStart()
    {
        Log.d(TAG, "onStart() called");
        super.onStart();
        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
            // ATTENTION: This was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            Action viewAction = Action.newAction(
                    Action.TYPE_VIEW, // TODO: choose an action type.
                    "ReqFormMap Page", // TODO: Define a title for the content shown.
                    // TODO: If you have web page content that matches this app activity's content,
                    // make sure this auto-generated web page URL is correct.
                    // Otherwise, set the URL to null.
                    Uri.parse("http://host/path"),
                    // TODO: Make sure this auto-generated app URL is correct.
                    Uri.parse("android-app://com.blogspot.whileinthisloop.petaplant/http/host/path")
            );
            AppIndex.AppIndexApi.start(mGoogleApiClient, viewAction);
        }
        //if (gpsOn)
        {
            Log.d(TAG, "3");
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            updateLocationOnMap(location);
        }
    }

    @Override
    protected void onResume()
    {
        Log.d(TAG, "onResume() called");
        super.onResume();
        //setUpMapIfNeeded();
        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
        }
    }
    @Override
    protected void onPause()
    {
        Log.d(TAG, "onPause() called");
        super.onPause();
        if (mGoogleApiClient != null)
        {
            if (mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        }

    }
    /*private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }*/
    protected void onStop()
    {
        Log.d(TAG, "onStop() called");
        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.disconnect();
            // ATTENTION: This was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            Action viewAction = Action.newAction(
                    Action.TYPE_VIEW, // TODO: choose an action type.
                    "ReqFormMap Page", // TODO: Define a title for the content shown.
                    // TODO: If you have web page content that matches this app activity's content,
                    // make sure this auto-generated web page URL is correct.
                    // Otherwise, set the URL to null.
                    Uri.parse("http://host/path"),
                    // TODO: Make sure this auto-generated app URL is correct.
                    Uri.parse("android-app://com.blogspot.whileinthisloop.petaplant/http/host/path")
            );
            AppIndex.AppIndexApi.end(mGoogleApiClient, viewAction);
        }
        super.onStop();
    }


    private void askForGPS()
    {
        Log.d(TAG, "in askForGps()");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5 * 1000) ;       // 10 seconds, in milliseconds

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        builder.setAlwaysShow(true);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                //.addConnectionCallbacks(this)
                //.addOnConnectionFailedListener(this)
                .addApi(AppIndex.API)
                .addApi(LocationServices.API)
                .build();

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        Log.d(TAG, "1");

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        gpsOn = true;
                        Log.d(TAG, "2");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(ReqFormMapActivity.this, GPS_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private boolean checkGpsPermission()
    {
        Log.d(TAG, "checkGpsPermission() called");
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }
    //onMapReady call back function
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        Log.d(TAG, "onMapReady() called");
        mMap = googleMap;

       /* // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
        setUpMap();
    }
    //if map is found then it is called
    private void setUpMap()
    {
        Log.d(TAG, "setUpMap() called");
        mMap.clear();
        //mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Marker"));
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.setMyLocationEnabled(true);
        if(checkGpsPermission()) //means ACCESS FINE LOCATION is permitted
        {
            mMap.setMyLocationEnabled(true);
        }
        else
        {
            onStop();
        }
    }

    //when google API client gets connected reads location and update it on map
    @Override
    public void onConnected(Bundle bundle)
    {
        Log.d(TAG, "onConnected() called");
        if (gpsOn)
        {
            Log.d(TAG, "3");
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            updateLocationOnMap(location);
        }
        else
        {
            onStop();
        }
    }

    //if location gets changed update location
    @Override
    public void onLocationChanged(Location location)
    {
        Log.d(TAG, "onLocationChanged() called");
        updateLocationOnMap(location);
    }

    //if connection gets failed
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
        Log.d(TAG, "onConnectionFailed() called");
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {}

    private void updateLocationOnMap(Location location)
    {
        Log.d(TAG, "updateLocationOnMap() called");
        Log.d(TAG, location.toString());
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
        //mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location"));
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("hi..")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        //mMap.clear();    //clears the previous location
        mMap.addMarker(options);
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.clear(); //clears the map at each time of update
        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(location.getLatitude(), location.getLongitude())).zoom(17).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == CAPTURE_PLANT_PLACE)
        {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user captured an image
                Bitmap bp = (Bitmap) data.getExtras().get("data");
                btnPlantPlace.setImageBitmap(bp);
            }
        }
    }
}
