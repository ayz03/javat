package com.blogspot.whileinthisloop.petaplant;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;

/**
 * Created by Ayaz on 23/7/2016.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, NavigatorActivity.class);
        startActivity(intent);
        finish();
    }
}