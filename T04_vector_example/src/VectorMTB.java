/**
 *this program shows that vector cant hold primitive data types so
 * wrapper class is used to do that vector grows/shrinks dynamically
 * @author Ayaz
 */

import java.util.*;
public class VectorMTB 
{
    public static void main(String[] args) 
    {
        Vector vMTB = new Vector();
        
        String Brand = "upland";
        float frame_size = 17.5f;
        boolean isBought = true;
        int price = 20000;
        char gears = '8';
        
        vMTB.addElement(Brand);
        
        Float F = new Float(frame_size);
        Boolean B = new Boolean(isBought);
        Integer I = new Integer(price);
        Character C = new Character(gears);
        
        vMTB.addElement(F);
        vMTB.addElement(B);
        vMTB.addElement(I);
        vMTB.addElement(C);
        
        System.out.println("my Mountain Bike's vector list is:");
        
        for (byte i = 0; i< vMTB.size(); i++) 
        {
            System.out.println(vMTB.elementAt(i));
        }
    }
    
}
