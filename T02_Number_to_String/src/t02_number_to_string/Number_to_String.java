//this program shows the importance of wrapper class and their methods 
//by wrapping a variable by it's Capital typed class you can do so many things 
package t02_number_to_string;

import org.json.simple.ItemList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ayaz
 */
public class Number_to_String 
{
    static ItemList myItemList = new ItemList("ABCDefghrijko");
    
    public static void main(String[] args) 
    {
        /*int n = 23;
        System.out.println("in Direct Printing: "+n);
        System.out.println("in String system: "+Integer.toString(n));
        System.out.println("in Decimal system: "+n);
        System.out.println("in Binary system: "+Integer.toBinaryString(n));
        System.out.println("in Hex system:0x"+Integer.toHexString(n));*/
        String myStrs[] = new String[25];
        myStrs = myItemList.getArray();
        
        for(int i=0; i<myStrs.length; i++)
        {
            System.out.println(myStrs[i]);   
        }
    }
}
