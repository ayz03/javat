/*
	The hello world demo
*/

#include <ets_sys.h>
#include <osapi.h>
#include <os_type.h>
#include <gpio.h>
#include "driver/uart.h"
#include <user_interface.h>

#define DELAY 1000 /* milliseconds */

//The macro LOCALis a synonym for the C language keyword "static"
LOCAL os_timer_t hello_timer;
LOCAL os_timer_t info_timer;
extern int ets_uart_printf(const char *fmt, ...);

/*
 * When running in user code, we need to be sensitive that the primary purpose of the device is
network communications. Since these are handled in the software, when user code gets control,
that simply means that networking code doesn't. Since we only have one thread of control, we
can't be in two places at once. The recommended duration to spend in user code at a single
sitting is less than 10msecs
 */
LOCAL void ICACHE_FLASH_ATTR hello_cb(void *arg)
{
	static int n = 0;
	ets_uart_printf("this is uart0\r\n");
	os_printf("this is uart1 counter: %d\r\n",n);
	n++;
	//now adding an infinite loop so that 10ms will overflow to see what happens
	//while(1)
	//{;}
}

LOCAL void ICACHE_FLASH_ATTR debug_mem_info(char* userNamePtr)
{
	os_printf("user name is %s \r\n",userNamePtr);
	system_print_meminfo();
}

//rf initialization parts reside here
void user_rf_pre_init(void)
{}

//this function will be called when initialization is completed
LOCAL void ICACHE_FLASH_ATTR initCompleted(void)
{
	os_printf("system initialization is complete\r\n");
	os_printf("flash size is: \r\n");
	enum flash_size_map flashSize = system_get_flash_size_map(); //this function depends on makefile info not the actual spi flash
	switch (flashSize)
	{
		case FLASH_SIZE_4M_MAP_256_256:
			os_printf("4MB: 256-256\r\n");
			break;
		case FLASH_SIZE_2M:
			os_printf("2MB: unknown\r\n");
			break;
		case FLASH_SIZE_8M_MAP_512_512:
			os_printf("8MB: 512-512\r\n");
			break;
		case FLASH_SIZE_16M_MAP_512_512:
			os_printf("16MB: 512-512\r\n");
			break;
		case FLASH_SIZE_32M_MAP_512_512:
			os_printf("32MB: 512-512\r\n");
			break;
		case FLASH_SIZE_16M_MAP_1024_1024:
			os_printf("16MB: 1024-1024\r\n");
			break;
		case FLASH_SIZE_32M_MAP_1024_1024:
			os_printf("32MB: 1024-1024\r\n");
			break;
		default:
			os_printf("unknown to enum set");
			break;
	}
}















//initial entry into the application
void user_init(void)
{
	// Configure the UART
	uart_init(BIT_RATE_115200, BIT_RATE_115200);
	// Set up a timer to send the message
	// os_timer_disarm(ETSTimer *ptimer)
	os_timer_disarm(&hello_timer);
	// os_timer_setfn(ETSTimer *ptimer, ETSTimerFunc *pfunction, void *parg)
	os_timer_setfn(&hello_timer, (os_timer_func_t *)hello_cb, (void *)0);
	// void os_timer_arm(ETSTimer *ptimer,uint32_t milliseconds, bool repeat_flag)
	os_timer_arm(&hello_timer, DELAY, 1);
	//Register a function to be called when system initialization is complete
	//void (*ptrFunc)(void);
	//ptrFunc = &initCompleted;
	//system_init_done_cb(ptrFunc);
	system_init_done_cb(&initCompleted);

	//creating new task to be run by timer
	char userName[5] = "ayaz";
	os_timer_disarm(&info_timer);
	os_timer_setfn(&info_timer, (os_timer_func_t*)debug_mem_info, (char*)userName);
	os_timer_arm(&info_timer, 2000, 1);
}


