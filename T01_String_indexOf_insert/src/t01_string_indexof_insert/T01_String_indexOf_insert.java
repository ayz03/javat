//a shor tutorial on String
package t01_string_indexof_insert;

/**
 *
 * @author Ayaz
 */
public class T01_String_indexOf_insert 
{
    public static void main(String[] args) 
    {
        StringBuffer s;
        s = new StringBuffer("I Object Oriented Programming");
        System.out.println("s is :"+s);
        int pos = s.indexOf("Object");
        s = s.insert(pos, "Love ");
        System.out.println("now s is: "+s);
    }
    
}
