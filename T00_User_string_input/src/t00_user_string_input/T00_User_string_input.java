/**
 *
 * @author Ayaz
 */
//this program inputs strings from user and shows it
package t00_user_string_input;
import java.io.*;
public class T00_User_string_input 
{
    public static void main (String args[]) throws IOException
    {
            String name;
            BufferedReader userInputReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter your name: ");
            name = userInputReader.readLine();
            System.out.print("your name is : ");
            System.out.println(name);
    }    
}

