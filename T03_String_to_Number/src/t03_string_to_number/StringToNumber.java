//this program shows how wrapper class can help to convert string to number 
package t03_string_to_number;
/**
 *
 * @author Ayaz
 */
import java.io.*;
public class StringToNumber 
{
    public static void main (String args[]) throws IOException
    {
            String userStr;
            BufferedReader userInputReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter a floating number: ");
            userStr = userInputReader.readLine();
            Float numF;
            numF = Float.valueOf(userStr);
            System.out.println("you have entered: "+numF);
    }    
    
}
